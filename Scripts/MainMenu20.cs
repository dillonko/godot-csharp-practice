using Godot;
using System;
using System.Threading;

public partial class MainMenu20 : Node2D
{
	[Export] public Label myLabel;
	[Export] public eSceneNames mySceneName;

	private int count = 0;

	public override void _Ready() {
		myLabel.Text = "Changed the lable on Ready in C#!";
	}

    public override void _Process(double delta){ }    

	public void _on_button_button_up() {

		SceneManager.instance.ChangeSene(eSceneNames.Main);

		count++;
		myLabel.Text = "The button changed the text. \n you clicked: " + count;
	}

}
