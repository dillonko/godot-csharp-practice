public class SceneData {
	 
	public SceneData(string path, string prettyName, bool pauseAllowed) {
		this.path = path;
		this.prettyName = prettyName;
		this.pauseAllowed = pauseAllowed;
	}

	// public string path; --> same as below
	public string path {set; get; }
	// public string prettyName; --> same as below
	public string prettyName { set; get; }
	// public bool pauseAllowed --> same as below
	public bool pauseAllowed { set; get; }

}
