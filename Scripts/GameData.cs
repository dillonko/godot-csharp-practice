using Godot;
using System;

public partial class GameData{

	//Video Settings
	public bool fullScreen =  true;
	public bool vsync = true;

	//Dialogue UI Settings
	public float dialogueBG_alpha = 0.4f;
	public int fontSizeMax = 11;

	//Audio Volumes
	public float masterMaxVolume = 1;
	public float musicMaxVolume = 0.40f;
	public float soundfxMaxVolume = 1;
	public float voiceMaxVolume = 1;
	public float femaleMaxVolume = 1;
	public float maleMaxVolume = 1;

	// Default Aidio Volumes
	public const float defualt_masterMaxVolume = 1;
	public const float defualt_musicMaxVolume = 0.40f;
	public const float defualt_soundfxMaxVolume = 1;
	public const float defualt_voiceMaxVolume = 1;
	public const float defualt_femaleMaxVolume = 1;
	public const float defualt_maleMaxVolume = 1;
	
	
}
